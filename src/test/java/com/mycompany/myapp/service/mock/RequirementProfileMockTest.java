package com.mycompany.myapp.service.mock;

import com.mycompany.myapp.domain.Candidate;
import com.mycompany.myapp.domain.RequirementProfile;
import com.mycompany.myapp.repository.CandidateRepository;
import com.mycompany.myapp.repository.RequirementProfileRepository;
import com.mycompany.myapp.service.CandidateService;
import com.mycompany.myapp.service.RequirementProfileService;
import com.mycompany.myapp.service.UserACL;
import com.mycompany.myapp.service.exception.AccessForbiddenException;
import com.mycompany.myapp.service.exception.BadRequestException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;
import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;

@ExtendWith(MockitoExtension.class)
public class RequirementProfileMockTest {

    @InjectMocks
    RequirementProfileService requirementProfileService;
    @Mock
    RequirementProfileRepository requirementProfileRepository;
    @Mock
    CandidateRepository candidateRepository;
    @Mock
    UserACL userACL;

    /**
     * Test create RequirementProfile from non admin permission user
     */
    @Test
    public void testCreateRequirementProfileWithNonAdmin(){
        //given
        given(userACL.isAdmin()).willReturn(false);
        // when
        AccessForbiddenException result = assertThrows(AccessForbiddenException.class, () -> requirementProfileService.createRequirementProfile(1));

        // then
        assertThat(result.getMessage()).isEqualTo("error.notAdmin");
    }
    /**
     * Test create RequirementProfile not found Candidate
     */
    @Test
    public void testCreateRequirementProfileNotFound(){
        //given
        given(userACL.isAdmin()).willReturn(true);
        //When
        BadRequestException result = assertThrows(BadRequestException.class, () -> requirementProfileService.createRequirementProfile(3));
        //Then
        assertThat(result.getMessage()).isEqualTo("Not_found");
    }

    /**
     * Test create RequirementProfile sucess
     */
    @Test
    public void testCreateRequirementProfile(){
        //given
        given(userACL.isAdmin()).willReturn(true);
        Candidate candidate = new Candidate();
        candidate.setId(1);
        candidate.setFirstName("Cuong");
        candidate.setLastName("Tran");
        candidate.setBirthYear("4000");
        RequirementProfile requirementProfile = new RequirementProfile();
        requirementProfile.setId(1);
        requirementProfile.setFirstName("Cuong");
        requirementProfile.setLastName("Tran");
        requirementProfile.setBirthYear("4000");
        given(candidateRepository.findById(eq(1))).willReturn(Optional.of(candidate));
        given(requirementProfileRepository.save(any(RequirementProfile.class))).willReturn(requirementProfile);
        //when
        RequirementProfile requirementProfile1 = requirementProfileService.createRequirementProfile(1);
        //Then
        assertThat(requirementProfile1.getId()).isEqualTo(1);
        assertThat(requirementProfile1.getFirstName()).isEqualTo("Cuong");
        assertThat(requirementProfile1.getLastName()).isEqualTo("Tran");
        assertThat(requirementProfile1.getBirthYear()).isEqualTo("4000");
    }

    /**
     * Test update RequirementProfile from non admin permission user
     */
    @Test
    public void testUpdateRequirementProfileWithNonAdmin(){
        //given
        given(userACL.isAdmin()).willReturn(false);
        RequirementProfile requirementProfile = new RequirementProfile();
        // when
        AccessForbiddenException result = assertThrows(AccessForbiddenException.class, () -> requirementProfileService.updateRequirementProfile(requirementProfile));

        // then
        assertThat(result.getMessage()).isEqualTo("error.notAdmin");
    }

    /**
     * Test update RequirementProfile success
     */
    @Test
    public void testUpdateRequirementProfile(){
        //given

        given(userACL.isAdmin()).willReturn(true);
        RequirementProfile requirementProfile = new RequirementProfile();
        requirementProfile.setId(1);
        requirementProfile.setFirstName("Tran");
        requirementProfile.setLastName("Cuong");

        RequirementProfile requirementProfile1 = new RequirementProfile();
        requirementProfile1.setId(1);
        requirementProfile1.setFirstName("My");
        requirementProfile1.setLastName("Duyen");
        given(requirementProfileRepository.findAll()).willReturn(Collections.singletonList(requirementProfile));
        given(requirementProfileRepository.save(any(RequirementProfile.class))).willReturn(requirementProfile1);
        // when
        RequirementProfile result =requirementProfileService.updateRequirementProfile(requirementProfile1);


        // then
        assertThat(result.getId()).isEqualTo(1);
        assertThat(result.getFirstName()).isEqualTo("My");
        assertThat(result.getLastName()).isEqualTo("Duyen");
    }


}
