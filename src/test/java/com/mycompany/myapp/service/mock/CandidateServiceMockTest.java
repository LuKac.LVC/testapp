package com.mycompany.myapp.service.mock;

import com.mycompany.myapp.domain.Candidate;
import com.mycompany.myapp.repository.CandidateRepository;
import com.mycompany.myapp.service.CandidateService;
import com.mycompany.myapp.service.UserACL;
import com.mycompany.myapp.service.exception.AccessForbiddenException;
import com.mycompany.myapp.service.exception.BadRequestException;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
@ExtendWith(MockitoExtension.class)
public class CandidateServiceMockTest {
    @InjectMocks
    CandidateService candidateService;
    @Mock
    CandidateRepository candidateRepository;
    @Mock
    UserACL userACL;

    /**
     * Import Candidate from non admin permission user
     */
    @Test
    public void testImportCandidateWithNonAdmin(){
        // given
        given(userACL.isAdmin()).willReturn(false);
        Candidate candidate = new Candidate();
        candidate.setLastName("Tran");
        candidate.setBirthYear("2001");
        // when
        AccessForbiddenException result = assertThrows(AccessForbiddenException.class, () -> candidateService.importCandidateManually(candidate));

        // then
        assertThat(result.getMessage()).isEqualTo("error.notAdmin");
    }
    /**
     * Import candidate manually with firtName null
     */
    @Test
    public void testImportCandidateFirtNameNull(){
        //given
        given(userACL.isAdmin()).willReturn(true);
        Candidate candidate = new Candidate();
        candidate.setLastName("Tran");
        candidate.setBirthYear("2001");

        //When
        BadRequestException result = assertThrows(BadRequestException.class, () -> candidateService.importCandidateManually(candidate));
        //then
        assertThat(result.getMessage()).isEqualTo("error.nullValue");
    }
    /**
     * Import candidate manually with LastName null
     */
    @Test
    public void testImportCandidateLastNameNull(){
        //given
        given(userACL.isAdmin()).willReturn(true);
        Candidate candidate = new Candidate();
        candidate.setFirstName("Cuong");
        candidate.setBirthYear("2001");
        //When
        BadRequestException result = assertThrows(BadRequestException.class, () -> candidateService.importCandidateManually(candidate));
        //then
        assertThat(result.getMessage()).isEqualTo("error.nullValue");
    }
    /**
     * BirthYear wrong format
     */
    @Test
    public void testImportCandidateBirthYearFormat(){
        //given
        given(userACL.isAdmin()).willReturn(true);
        Candidate candidate = new Candidate();
        candidate.setFirstName("Cuong");
        candidate.setLastName("Tran");
        candidate.setBirthYear("4000");
        //When
        BadRequestException result = assertThrows(BadRequestException.class, () -> candidateService.importCandidateManually(candidate));
        //then
        assertThat(result.getMessage()).isEqualTo("error.invalidYear");
    }
    /**
     * Import Candidate Success
     */
    @Test
    public void testImortCandidateSuccess(){
        //given
        given(userACL.isAdmin()).willReturn(true);
        Candidate candidate = new Candidate();
        candidate.setFirstName("Cuong");
        candidate.setLastName("Tran");
        candidate.setBirthYear("2001");
        //When
        Candidate candidate1 = candidateService.importCandidateManually(candidate);
        //then
        assertThat(candidate1.getFirstName()).isEqualTo("Cuong");
        assertThat(candidate1.getLastName()).isEqualTo("Tran");
        assertThat(candidate1.getBirthYear()).isEqualTo("2001");
    }
    /**
     * Import Candidate from excel success
     */
    @Test
    public void testImortCandidateExcelSuccess() throws FileNotFoundException {
        //given
        given(userACL.isAdmin()).willReturn(true);
        File file = new File("src/main/resources/CreateCandidate.xlsx");
        //When
        List<Candidate> list = candidateService.importCandidateByExcel(new FileInputStream(file));
        //then
        assertThat(list.size()).isEqualTo(6);
        assertThat(list.get(0).getBirthYear()).isEqualTo("2001");
    }
    /**
     * get list Candidate paging
     */
    @Test
    public void getAllCadidate() throws FileNotFoundException {
        //given
        given(userACL.isAdmin()).willReturn(true);
        File file = new File("src/main/resources/CreateCandidate.xlsx");
        Sort sort = Sort.by(Sort.Direction.ASC,"lastName");
        Pageable pageable = PageRequest.of(1,2,sort);
        List<Candidate> list = candidateService.importCandidateByExcel(new FileInputStream(file));
        given(candidateRepository.findAll(pageable)).willReturn(new PageImpl<>(list.subList(0,2),pageable,list.size()));
        Optional<String> field = Optional.of("lastName");

        //When
        Page<Candidate> page = candidateService.getAllCandidateSort(1,2,field);
        //then
        assertThat(page.getContent().size()).isEqualTo(2);
        assertThat(page.getContent().get(0).getLastName()).isEqualTo("Cuong");
        assertThat(page.getTotalPages()).isEqualTo(3);

    }

}
