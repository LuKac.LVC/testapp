package com.mycompany.myapp.domain;

import javax.persistence.*;
import javax.validation.constraints.Size;
import lombok.*;

@Entity
@Table(name = "candidate")
public class Candidate {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "first_name", nullable = false)
    private String firstName;

    @Column(name = "last_name", nullable = false)
    private String lastName;

    @Column(name = "birth_year")
    private String birthYear;

    @Column(name = "personal_number")
    private String personalNumber;

    @Column
    private String gender;

    @Column
    private String region;

    @Column
    private String country;

    @Column(name = "linkedIn_link")
    private String linkedInLink;

    @Column
    private String employer;

    @Column(name = "first_employment")
    private String firstEmployment;

    @Column(name = "managerial_position")
    private String managerialPosition;

    @Column(name = "business_areas")
    private String businessAreas;

    @Column(name = "cvs")
    private String cVS;

    @Column
    private String contact;

    public Candidate() {
    }

    public Candidate(String firstName, String lastName, String birthYear, String personalNumber, String gender, String region, String country, String linkedInLink, String employer, String firstEmployment, String managerialPosition, String businessAreas, String cVS, String contact) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthYear = birthYear;
        this.personalNumber = personalNumber;
        this.gender = gender;
        this.region = region;
        this.country = country;
        this.linkedInLink = linkedInLink;
        this.employer = employer;
        this.firstEmployment = firstEmployment;
        this.managerialPosition = managerialPosition;
        this.businessAreas = businessAreas;
        this.cVS = cVS;
        this.contact = contact;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getBirthYear() {
        return birthYear;
    }

    public void setBirthYear(String birthYear) {
        this.birthYear = birthYear;
    }

    public String getPersonalNumber() {
        return personalNumber;
    }

    public void setPersonalNumber(String personalNumber) {
        this.personalNumber = personalNumber;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getLinkedInLink() {
        return linkedInLink;
    }

    public void setLinkedInLink(String linkedInLink) {
        this.linkedInLink = linkedInLink;
    }

    public String getEmployer() {
        return employer;
    }

    public void setEmployer(String employer) {
        this.employer = employer;
    }

    public String getFirstEmployment() {
        return firstEmployment;
    }

    public void setFirstEmployment(String firstEmployment) {
        this.firstEmployment = firstEmployment;
    }

    public String getManagerialPosition() {
        return managerialPosition;
    }

    public void setManagerialPosition(String managerialPosition) {
        this.managerialPosition = managerialPosition;
    }

    public String getBusinessAreas() {
        return businessAreas;
    }

    public void setBusinessAreas(String businessAreas) {
        this.businessAreas = businessAreas;
    }

    public String getcVS() {
        return cVS;
    }

    public void setcVS(String cVS) {
        this.cVS = cVS;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }
}
