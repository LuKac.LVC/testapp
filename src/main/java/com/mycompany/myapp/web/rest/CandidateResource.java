package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.domain.Candidate;
import com.mycompany.myapp.security.AuthoritiesConstants;
import com.mycompany.myapp.service.CandidateService;
import com.mycompany.myapp.service.exception.BadRequestException;
import com.mycompany.myapp.web.rest.errors.EmailAlreadyUsedException;
import com.mycompany.myapp.web.rest.errors.InvalidPasswordException;
import com.mycompany.myapp.web.rest.errors.LoginAlreadyUsedException;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/candidate")
public class CandidateResource {

    @Autowired
    private CandidateService candidateService;

    /**
     * {@code Get  /} : get candidates data in list and sort by field,pagination
     */
    @GetMapping("")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<?> getAllCandidate(@RequestParam("page") Integer page,@RequestParam("size") Integer size,@RequestParam("field")Optional<String> field) {
        Page<Candidate> candidateList = candidateService.getAllCandidateSort(page,size,field);
        return ResponseEntity.ok(candidateList.getContent());
    }
    /**
     * {@code POST  /manually} : create candidate information manually.
     * @throws BadRequestException {@code 400 (Bad Request)} if firstName and lastName is null.
     * @throws BadRequestException {@code 400 (Bad Request)} If the date of birth is outside of the range 1000 to 2999.
     */
    @PostMapping("/manually")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<?> createCandidateManually(@RequestBody Candidate candidate) {
        Candidate result = candidateService.importCandidateManually(candidate);
        return ResponseEntity.ok(result);
    }
    /**
     * {@code POST  /excel} : create candidate information form file excel.
     * @throws BadRequestException {@code 400 (Bad Request)} if firstName and lastName is null.
     * @throws BadRequestException {@code 400 (Bad Request)} If the date of birth is outside of the range 1000 to 2999.
     */
    @PostMapping("/excel")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<?> createCandidateExcel(@RequestParam(name = "file", required = true) MultipartFile file) throws IOException {
        List<Candidate> result = candidateService.importCandidateByExcel(file.getInputStream());
        return ResponseEntity.ok(result);
    }

    /**
     * {@Code Get /search} : Search all Candidates with keyword related fields
     * @param keyword
     * @return
     */
    @GetMapping("/search/keyword")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<?> searchByKeyWord(@RequestParam(name = "page") Integer page,@RequestParam(name = "keyword") String keyword){
        Page<Candidate> result = candidateService.findCandidateByKeyword(page,keyword);
        return ResponseEntity.ok(result.getContent());
    }
    @GetMapping("/search/text")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<?> searchByText(@RequestParam(name = "page") Integer page,@RequestParam(name = "text") String text){
        Page<Candidate> result = candidateService.findCandidateByText(page,text);
        return ResponseEntity.ok(result.getContent());
    }
}
