package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.domain.Candidate;
import com.mycompany.myapp.domain.RequirementProfile;
import com.mycompany.myapp.repository.RequirementProfileRepository;
import com.mycompany.myapp.security.AuthoritiesConstants;
import com.mycompany.myapp.service.RequirementProfileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/requirementProfile")
public class RequirementProfileResource {
    @Autowired
    private RequirementProfileService requirementProfileService;
    @PostMapping("/create")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<?> createRequirementProfile(@RequestParam(name = "idCandidate") Integer idCandidate) {
        RequirementProfile requirementProfile = requirementProfileService.createRequirementProfile(idCandidate);
        return ResponseEntity.ok(requirementProfile);
    }
    @PutMapping("/update")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<?> updateRequirementProfile(@RequestBody RequirementProfile requirementProfile){
        RequirementProfile requirementProfile1 = requirementProfileService.updateRequirementProfile(requirementProfile);
        return  ResponseEntity.ok(requirementProfile1);
    }
    @DeleteMapping("/delete")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<?> deleteRequirementProfile(@RequestParam(name = "id") Integer id){
        RequirementProfile requirementProfile = requirementProfileService.deteleRequirementProfile(id);
        return ResponseEntity.ok(requirementProfile);
    }
    @GetMapping("")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.USER + "\")")
    public ResponseEntity<?> getAllRequirementProfile(@RequestParam("page") Integer page,@RequestParam("size") Integer size,@RequestParam("field") Optional<String> field) {
        Page<RequirementProfile> candidateList = requirementProfileService.getAllRequirementProfileSort(page,size,field);
        return ResponseEntity.ok(candidateList.getContent());
    }
}
