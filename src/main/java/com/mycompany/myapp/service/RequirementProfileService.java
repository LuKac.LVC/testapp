package com.mycompany.myapp.service;

import com.mycompany.myapp.domain.Candidate;
import com.mycompany.myapp.domain.RequirementProfile;
import com.mycompany.myapp.repository.CandidateRepository;
import com.mycompany.myapp.repository.RequirementProfileRepository;
import com.mycompany.myapp.service.exception.AccessForbiddenException;
import com.mycompany.myapp.service.exception.BadRequestException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class RequirementProfileService {
    @Autowired
    private RequirementProfileRepository requirementProfileRepository;
    @Autowired
    private CandidateRepository candidateRepository;
    @Autowired
    private UserACL userACL;
    public List<RequirementProfile>  getAllRequirementProfile(){
        return (List<RequirementProfile>) requirementProfileRepository.findAll();
    }
    public RequirementProfile createRequirementProfile(Integer idCandidate){
        if (!userACL.isAdmin()) {
            throw new AccessForbiddenException("error.notAdmin");
        }
        try {
            Candidate candidate = candidateRepository.findById(idCandidate).get();

            RequirementProfile requirementProfile = new RequirementProfile();
            requirementProfile.setFirstName(candidate.getFirstName());
            requirementProfile.setLastName(candidate.getLastName());
            requirementProfile.setBirthYear(candidate.getBirthYear());
            requirementProfile.setPersonalNumber(candidate.getPersonalNumber());
            requirementProfile.setGender(candidate.getGender());
            requirementProfile.setRegion(candidate.getRegion());
            requirementProfile.setCountry(candidate.getCountry());
            requirementProfile.setLinkedInLink(candidate.getLinkedInLink());
            requirementProfile.setEmployer(candidate.getEmployer());
            requirementProfile.setFirstEmployment(candidate.getFirstEmployment());
            requirementProfile.setManagerialPosition(candidate.getManagerialPosition());
            requirementProfile.setBusinessAreas(candidate.getBusinessAreas());
            requirementProfile.setcVS(candidate.getcVS());
            requirementProfile.setContact(candidate.getContact());

            return  requirementProfileRepository.save(requirementProfile);
        }
        catch (Exception e){
            throw new BadRequestException("Not_found",null);
        }

    }

    public RequirementProfile updateRequirementProfile(RequirementProfile requirementProfile){
        if (!userACL.isAdmin()) {
            throw new AccessForbiddenException("error.notAdmin");
        }
        List<RequirementProfile> list = getAllRequirementProfile();
        for (RequirementProfile requirementProfile1: list){
            if(requirementProfile1.getId()== requirementProfile.getId()){
                return requirementProfileRepository.save(requirementProfile);
            }
        }
        throw new BadRequestException("Not_found",null);
    }
    public RequirementProfile deteleRequirementProfile(Integer id){
        if (!userACL.isAdmin()) {
            throw new AccessForbiddenException("error.notAdmin");
        }
        List<RequirementProfile> list = getAllRequirementProfile();
        for (RequirementProfile requirementProfile1: list){
            if(requirementProfile1.getId()== id){
                RequirementProfile requirementProfile = requirementProfile1;
                requirementProfileRepository.deleteById(id);
                return requirementProfile;
            }
        }
        throw new BadRequestException("Not_found",null);
    }

    public Page<RequirementProfile> getAllRequirementProfileSort(Integer page, Integer size, Optional<String> field) {
        if(!userACL.isUser()){
            throw new AccessForbiddenException("error.notUser");
        }
        Sort sort = Sort.by(Sort.Direction.ASC,field.orElse("id"));
        Pageable pageable = PageRequest.of(page, size, sort);
        return requirementProfileRepository.findAll(pageable);
    }
}
