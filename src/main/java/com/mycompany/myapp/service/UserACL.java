package com.mycompany.myapp.service;

import com.mycompany.myapp.security.AuthoritiesConstants;
import com.mycompany.myapp.security.SecurityUtils;
import org.springframework.stereotype.Service;

@Service
public class UserACL {
    /**
     * Check if current user is admin
     * @return true if user is logged in and has role 'admin', false otherwise
     */
    public boolean isAdmin() {

        return SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.ADMIN);
    }
    public boolean isUser(){
        return SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.USER);
    }
}
