package com.mycompany.myapp.service;

import com.mycompany.myapp.domain.Candidate;
import com.mycompany.myapp.repository.CandidateRepository;
import com.mycompany.myapp.service.exception.AccessForbiddenException;
import com.mycompany.myapp.service.exception.BadRequestException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;

@Service
public class CandidateService {

    @Autowired
    private CandidateRepository candidateRepository;
    @Autowired
    private  UserACL userACL;
    /**
        import Candidate manually
     */
    public Candidate importCandidateManually(Candidate candidate) {
        if (!userACL.isAdmin()) {
            throw new AccessForbiddenException("error.notAdmin");
        }
        if (candidate.getFirstName() == null || candidate.getLastName() == null) {
            throw new BadRequestException("error.nullValue", null);
        }
        if (!candidate.getBirthYear().matches("[12][0-9]{3}")) {
            throw new BadRequestException("error.invalidYear", null);
        }
        candidateRepository.save(candidate);
        return candidate;
    }

    /**
     import Candidate from file excel
     */
    public List<Candidate> importCandidateByExcel(InputStream input) {
        if (!userACL.isAdmin()) {
            throw new AccessForbiddenException("error.notAdmin");
        }
        List<Candidate> candidateList = new ArrayList<>();
        try {
            Workbook workbook = new XSSFWorkbook(input);
            Sheet sheet = workbook.getSheetAt(0);
            Iterator<Row> rows = sheet.iterator();
            rows.next();
            while (rows.hasNext()) {
                Row row = rows.next();
                Candidate candidate = new Candidate();
                if (row.getCell(0).getCellType() == CellType.STRING) {
                    candidate.setFirstName(row.getCell(0).getStringCellValue());
                }
                if (row.getCell(1).getCellType() == CellType.STRING) {
                    candidate.setLastName(row.getCell(1).getStringCellValue());
                }
                if (row.getCell(2).getCellType() == CellType.STRING) {
                    candidate.setBirthYear(row.getCell(2).getStringCellValue());
                }
                if (row.getCell(3).getCellType() == CellType.STRING) {
                    candidate.setPersonalNumber(row.getCell(3).getStringCellValue());
                }
                if (row.getCell(4).getCellType() == CellType.STRING) {
                    candidate.setGender(row.getCell(4).getStringCellValue());
                }
                if (row.getCell(5).getCellType() == CellType.STRING) {
                    candidate.setRegion(row.getCell(5).getStringCellValue());
                }
                if (row.getCell(6).getCellType() == CellType.STRING) {
                    candidate.setCountry(row.getCell(6).getStringCellValue());
                }
                if (row.getCell(7).getCellType() == CellType.STRING) {
                    candidate.setLinkedInLink(row.getCell(7).getStringCellValue());
                }
                if (row.getCell(8).getCellType() == CellType.STRING) {
                    candidate.setEmployer(row.getCell(8).getStringCellValue());
                }
                if (row.getCell(9).getCellType() == CellType.STRING) {
                    candidate.setFirstEmployment(row.getCell(9).getStringCellValue());
                }
                if (row.getCell(10).getCellType() == CellType.STRING) {
                    candidate.setManagerialPosition(row.getCell(10).getStringCellValue());
                }
                if (row.getCell(11).getCellType() == CellType.STRING) {
                    candidate.setBusinessAreas(row.getCell(11).getStringCellValue());
                }
                if (row.getCell(12).getCellType() == CellType.STRING) {
                    candidate.setcVS(row.getCell(12).getStringCellValue());
                }
                if (row.getCell(13).getCellType() == CellType.STRING) {
                    candidate.setContact(row.getCell(13).getStringCellValue());
                }
                this.importCandidateManually(candidate);
                candidateList.add(candidate);
            }
            workbook.close();
            input.close();
            return candidateList;
        } catch (IOException e) {
            throw new BadRequestException("error.uploadExcelWrongFormat", null);
        }
    }

    public List<Candidate> getAllCandidate(){
        return (List<Candidate>) candidateRepository.findAll();
    }
    /**
     * get all Candidate and sort by field,pagination
     */
    public Page<Candidate> getAllCandidateSort(Integer page, Integer size, Optional<String> field) {
        if (!userACL.isAdmin()) {
            throw new AccessForbiddenException("error.notAdmin");
        }
        Sort sort = Sort.by(Sort.Direction.ASC,field.orElse("id"));
        Pageable pageable = PageRequest.of(page, size, sort);
        return candidateRepository.findAll(pageable);
    }

    /**
         * Search all Candidates with keyword related fields
     * @param page
     * @param keyword
     * @return
     */
    public Page<Candidate> findCandidateByKeyword(Integer page,String keyword){
        if (!userACL.isAdmin()) {
            throw new AccessForbiddenException("error.notAdmin");
        }
        Pageable pageable = PageRequest.of(page,10);
        return candidateRepository.findAll(pageable,keyword);
    }

    /**
     *
     * Search for candidates with information about the fields related to the text
     * @param page
     * @param text
     * @return
     */
    public Page<Candidate> findCandidateByText(Integer page,String text){
        if (!userACL.isAdmin()) {
            throw new AccessForbiddenException("error.notAdmin");
        }
        List<Candidate> list = (List<Candidate>) candidateRepository.findAll();
        List<Candidate> result = new ArrayList<>();
        text=text.toLowerCase();
        for (Candidate candidate: list){
            if(candidate.getFirstName()!=null && !candidate.getFirstName().isEmpty() && (text.contains(candidate.getFirstName().toLowerCase()))
                || (candidate.getLastName()!=null && !candidate.getLastName().isEmpty() && text.contains(candidate.getLastName().toLowerCase()) )
                || ( candidate.getBirthYear()!=null && !candidate.getBirthYear().isEmpty() && text.contains(candidate.getBirthYear().toLowerCase()))
                || (candidate.getPersonalNumber()!=null && !candidate.getPersonalNumber().isEmpty() && text.contains(candidate.getPersonalNumber().toLowerCase()))
                || (candidate.getGender()!=null && !candidate.getGender().isEmpty() && text.contains(candidate.getGender().toLowerCase()))
                || (candidate.getRegion()!=null && !candidate.getRegion().isEmpty() && text.contains(candidate.getRegion().toLowerCase()) )
                || (candidate.getCountry()!=null && !candidate.getCountry().isEmpty() && text.contains(candidate.getCountry().toLowerCase()))
                || (candidate.getLinkedInLink()!=null && !candidate.getLinkedInLink().isEmpty() && text.contains(candidate.getLinkedInLink().toLowerCase()) )
                || (candidate.getEmployer()!=null && !candidate.getEmployer().isEmpty() && text.contains(candidate.getEmployer().toLowerCase()) )
                || (candidate.getFirstEmployment()!=null && !candidate.getFirstEmployment().isEmpty()  && text.contains(candidate.getFirstEmployment().toLowerCase()))
                || (candidate.getManagerialPosition()!=null && !candidate.getManagerialPosition().isEmpty() && text.contains(candidate.getManagerialPosition().toLowerCase()))
                || (candidate.getcVS()!=null && !candidate.getcVS().isEmpty() && text.contains(candidate.getcVS().toLowerCase()) )
                || (candidate.getContact()!=null && !candidate.getContact().isEmpty() && text.contains(candidate.getContact().toLowerCase()))
            )
            {
               result.add(candidate) ;
            }
        }
        Page<Candidate> page1 = new PageImpl<>(new ArrayList<>());
        if((page+1)*10<=result.size()){
             page1 = new PageImpl<>(result.subList(page*10,(page+1)*10));
            return page1;
        }else {
            page1 = new PageImpl<>(result.subList(page*10,result.size()));
            return page1;
        }
    }
    public Candidate getCandidateById(Integer id){

        return  candidateRepository.findById(id).get();
    }

}
