package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.RequirementProfile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface RequirementProfileRepository extends PagingAndSortingRepository<RequirementProfile,Integer> {
}
