package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.Candidate;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface CandidateRepository extends PagingAndSortingRepository<Candidate, Integer> {
    @Query("select c from Candidate c where c.firstName like %?1%"
            + "or c.lastName like %?1%"
            + "or c.birthYear like %?1%"
            + "or c.personalNumber like %?1%"
            + "or c.gender like %?1%"
            + "or c.region like %?1%"
            + "or c.country like %?1%"
            + "or c.linkedInLink like %?1%"
            + "or c.employer like %?1%"
            + "or c.firstEmployment like %?1%"
            + "or c.managerialPosition like %?1%"
            + "or c.businessAreas like %?1%"
            + "or c.cVS like %?1%"
            + "or c.contact like %?1%"
    )
    public Page<Candidate> findAll(Pageable pageable, String keyword);

}
